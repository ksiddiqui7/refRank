#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#refRank provides a coverage-based ranking of references based on read mapping of multiple datasets [NGS]
#Copyright (C) 2017  Stephan Fuchs, RKI (FG13), fuchss@rki.de

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

version = '1.0.0'

import time
import sys
import os
import subprocess
import shutil
import mimetypes
import gzip
import random
import re

#ouput class
class output():
	'''
	Some functions to create, modify, or delete files & folders
	''' 
	
	
	@staticmethod
	def createDir(dirname = False, addtimestmp = False, silent = False):
		''' 
		Creates folder or use empty folder.
		Existing folders which are not empty induce error message and exit. 
		Timestamp can be added to folder name. 
		Returns folder name.
		
		Input:
		dirname 	designated folder name
		addtimestmp	add timestamp (YYYY-MM-DD_hh-mm-ss) to folder name (True | False)
		silent		show no messages except errors (True | False)
		
		Return:
		path to folder
		'''		
		
		#msg
		if not silent:
			print("preparing output folder ...")
		
		#folder name given
		if dirname and not addtimestmp:
			if os.path.isdir(dirname) == True and len(os.listdir(dirname)) != 0:
				sys.exit("ERROR: the output directory is not empty!")
		
		#folder name not given						
		else:
			dirpref = dirname
			err = 0
			while True:
				timecode = time.strftime("%Y-%m-%d_%H-%M-%S")
				dirname = dirpref + '_' + timecode 
				if os.path.isdir(dirname):
					err += 1
				else:
					break
				if err == 30:
					sys.exit("ERROR: standard folder names already existing")
		
		os.makedirs(dirname)
		#msg
		if not silent:
			print(" ", dirname)	
		return dirname
		
					
	@staticmethod
	def createFile(filename, mode = 'w', owr = False, ask= True, silent = False):
		''' 
		creates files using given mode
		if file already exists, user can be asked for overwriting
		
		Input:
		filename	desginated file name
		mode		opening more
		owr			overwriting allowed
		ask			ask for user input before overwriting
		
		Return:
		file handle
		'''		
		
		#msg
		if not silent:
			print("preparing output file...")
		
		#file not existent or overwriting active
		if owr == True or os.path.isfile(filename) == False:
			pass
				
		elif os.path.isfile(filename):
			#asking
			if ask == True:
				userinput = str(input("Output file exists! Should it be overwritten? [Y/n]:  "))
				if userinput == 'Y':
					pass
				else:
					sys.exit('Program terminated.')
			#not asking
			else:
				sys.exit('ERROR: Output file exists! Program terminated.')
			
		#msg
		if not silent:
			print(" ", filename)		
				
		return open(filename, mode)
		
	@staticmethod
	def logging(line, filename, lb = "\n", mode="a"):
		'''
		writes text to file.
		By default file is opened using 'a' mode

		
		Input:
		line		text to write
		filename	file to be appended
		lb			line break
		mode		file opening mode
				
		Return:
		True
		'''	
		
		with open(filename, 'a') as handle:	
			handle.write(line + lb)	
			
		return True
		
	@staticmethod
	def formatAsCols (input, sep = ' ' * 5, lb = "\n"):
		'''
		formats list of lists in columns.
		List contains lines as lists containing columns as elements.
		'''
		
		
		#allowed signs in numbers
		pattern = re.compile(r'([+-])?([0-9]+)([.,]?)([0-9]+)(%?)')

		#str conversion
		lines = [[str(x) for x in line] for line in input] 

		#fill up rows to same number of columns (important for transposing)
		maxlen = max([len(x) for x in lines])
		[x.extend(['']*(maxlen-len(x))) for x in lines]    
		
		#find format parameters
		width = [] #colum width or length
		align = [] #alignment type (number = r; strings = l)
		prec = [] #number precision
		for column in zip(*lines):
			width.append(len(column[0]))
			prec.append(0)
			align.append('r')
			for field in column[1:]:
				if align[-1] ==  'l':
					width[-1] = max(width[-1], len(field))
				else:
					match = pattern.match(field)
					if match and match.group(0) == field:
						if match.group(3) and match.group(4):
							prec[-1] = max(prec[-1], len(match.group(4)))
						
						if prec[-1] > 0:
							k = 1
						else:
							k = 0
						width[-1] = max(width[-1], len(match.group(2)) + prec[-1] + k + len(match.group(4)))
					else:
						align[-1] = 'l'
						prec[-1] = False
						width[-1] = max(width[-1], len(field))
				
		#formatting
		output = []
		for	line in lines:
			f = 0
			output.append([])
			for field in line:
				if align[f] == 'l':
					output[-1].append(field + ' ' * (width[f] - len(field)))
				elif align[f] == 'r':
					match = pattern.match(field)
					if not match or match.group(0) != field: 
						output[-1].append(' ' * (width[f] - len(field)) + field)
					else:
						if match.group(5):
							percent = match.group(5)
						else:
							percent = ''
						length = len(percent)
						
						intpart = match.group(2)
						if match.group(1):
							intpart = match.group(1) + intpart
						if match.group(3):
							decpart = match.group(4) 
						else:
							intpart += match.group(4)
							decpart = ''							
						length += len(intpart)
						
						if prec[f] > 0:
							decpart += '0' * (prec[f] - len(match.group(4)))
							length += len(decpart) + 1
							formatted_number = ' ' * (width[f] - length) + intpart + '.' + decpart + percent
						else:
							formatted_number = ' ' * (width[f] - length) + intpart + percent
						output[-1].append(formatted_number)		
				f += 1
		
		return lb.join([sep.join(x) for x in output])
				
		

# refrank class #
class refrank():

	# initialization #	
	def __init__(self, reffilenames, fastqfilenames, readpairing, fraction, mapper, q, Q, keepBAM = False, keepSubset = False, resultdir = False, tmpdir = False, traceless = False, cpus = 1, silent = False):	
		'''
		initializates the refrank object
		creates a workspace consisting of 
			result folder (by default: REFRANK_{TIMPESTAMP}) 
			temp folder (by default: result folder/tmp)
		adds references to reference collection
		adds datasets to dataset collection
		
		Set Attributes:
		self.inittime
		self.readpairing
		self.traceless
		self.resultdir
		self.tmpdir
		
		Input:
		reffilenames		list of reference FASTA files (exactly one FASTA entry per file allowed)
		fastqfilenames		list of fastq file names (paired end-mode order has to be: fwd_data1, rev_data1, fwd_data2, rev_data2, ...
		readparing			paired-end read data (True | False)
		fraction			percentage (float) or absolute read number (integer) 
		mapper				designated mapper (bwamem | bwasw)
		q					base quality threshold (int)
		Q					mapping quality threshold (int)
		keepBAM				store BAM files (True | False)
		keepSubset			store downsampled Fastq files (True | False)		
		resultdir			folder to save all results (has to be non-existent or empty)
		tmpdir				folder to save all temporary data (has to be non-existent or empty)
		traceless			delete all created folders and files if object is deleted or script ends
		cpus				number of cpus to use
		silent				do not print messages except errors (True | False)		
		'''
			
		
		#add attributes
		self.inittime = time.time()
		self.readpairing = readpairing
		self.traceless = traceless
		self.q = q
		self.Q = Q		
		
		###create workspace			
		self.resultdir 	= self.newDir(resultdir, silent = silent)
		if tmpdir:
			self.tmpdir = self.newDir(tmpdir, silent = True)
		else:
			self.tmpdir = self.newDir(os.path.join(self.resultdir, "tmp"), silent = True)
		
		#process reference data
		self.references = []
		i = 0
		t = str(len(reffilenames))
		while reffilenames:
			i += 1
			print ("\rprocessing reference files ... [" + str(i) + "/" + t + "]      ", end=" ")
			self.addRef(reffilenames.pop(0), silent=True)
		print()
			
		#process fastq files
		self.datasets = []
		i = 0
		t = str(len(fastqfilenames))		
		while fastqfilenames:
			i += 1
			filename = fastqfilenames.pop(0)
			if readpairing:
				i += 1
				filename2 = fastqfilenames.pop(0)
			else:
				filename2 = False
			print ("\rprocessing fastq files ... [" + str(i) + "/" + t + "]      ", end=" ")
			self.addFastq(filename, filename2, silent = True)
		print()
		
		#ranking
		self.performRanking(fractionsize, mapper, q, Q, keepBAM, keepSubset, cpus, silent=silent)
		
		#result repor
		self.createResultReport(mapper, fraction, silent=silent)
		
		#delete dirs in traceless mode
		if self.traceless:
			self.delTraces()
			
	def __enter__(self):
		return self			
		
	
	# new dir creation #
	def newDir(self, dirname = False, silent = False):
		'''
		uses a empty folder or creates non-existing result folder (by default: REFRANK_{TIMPESTAMP}) 
		and sets attribute self.resultdir

		Input:
		dirname		designated name of folder
		silent		show no messages except errors (True | False) 
		
		Output:
		True
		'''
		
		if not dirname:
			dirname = os.path.abspath(output.createDir('REFRANK', addtimestmp = True, silent = silent))
		else:
			dirname = os.path.abspath(output.createDir(dirname, silent = silent))
			
		return dirname
		
		
	# ranking #
	def performRanking(self, fraction, mapper, q, Q, keepBAM = False, keepSubset = False, cpus = 1, silent = False):
		'''
		extends workspace for
			BAM files if necessary
			subset FastQ files if necessary 

		Set Attributes:
		self.bamdir
		self.fastqdir
		
		Input:
		fraction		percentage (float) or absolute read number (integer) 
		mapper			designated mapper (bwamem | bwasw)
		q				base quality threshold (int)
		Q				mapping quality threshold (int)
		keepBAM			store BAM files (True | False)
		keepSubset		store downsampled Fastq files (True | False)
		silent			show no messages except errors (True | False) 		

		Return:
		True
		
		'''	
		
		##extend workspace and create files
		
		###bam file index
		if keepBAM:
			self.bamdir = output.createDir(os.path.join(self.resultdir, "BAMs"), silent=True)
			bamfileindex = [["task", "BAM file", "reference file", "reference header", "fastq file(s)"]]
		else:
			self.bamdir = self.tmpdir

		###subset fastq file index
		if keepSubset:			
			self.fastqdir = output.createDir(os.path.join(self.resultdir, "FASTQs"), silent=True)
			fastqfileindex = [["fastq file", "read count", "parent file", "parent read count"]]
		else:
			self.fastqdir = self.tmpdir
			
		###process log file
		processlogfilename = os.path.join(self.resultdir, 'process.log')
		output.logging('# START LOG #', processlogfilename)	
		
		##indexing
		i = 0
		t = str(len(self.references))
		for filename in [x[0] for x in self.references]:
			i += 1
			print ("\rindexing reference files ... [" + str(i) + "/" + t + "]      ", end=" ")
			output.logging('# INDEXING ' + os.path.basename(filename) + '#', processlogfilename)	
			refrank.indexRef(filename, mapper, processlogfilename, silent = True)
		print()
			
		##main actions
		self.coverages = {}
		
		current_task = 0
		total_tasks = len(self.references) * len(self.datasets) 

		####dataset loop
		for dataset in self.datasets:
			print ("preparing next dataset...")
			if readpairing:
				print("  paired-end read data (forward): " + os.path.basename(dataset[0]))
				print("  paired-end read data (reverse): " + os.path.basename(dataset[1]))
			else:
				print("  single read data: " + os.path.basename(dataset[0]))
				
			print("  read count: " + str(dataset[2]) + " per file")
				
			###downsampling FASTQ files
			srcfiles = [dataset[0]]
			dstfiles = [self.nameSubsetFASTQ(dataset[0])]
			if readpairing:
				srcfiles.append(dataset[1])
				dstfiles.append(self.nameSubsetFASTQ(dataset[1]))
				
			subsetsize = refrank.createFastqSubset(srcfiles, dstfiles, fraction, dataset[2], silent = silent)
			
			print("    fraction: " + str(subsetsize) + " reads per file")
			
			if keepSubset:
				i = -1
				for srcfile in srcfiles:
					i += 1
					fastqfileindex.append([os.path.basename(dstfiles[i]), subsetsize, os.path.basename(srcfile), dataset[2]])
					
			if readpairing:
				readnumber = subsetsize*2	
			else:
				readnumber = subsetsize					
				
			self.coverages[srcfiles[0]] = {}		

			####ref loop
			for reffilename, refheader, reflen in self.references:		
				###preparing new task
				current_task += 1
				print ('TASK', current_task, 'OF', total_tasks)
				
				samfile = os.path.join(self.tmpdir, str(current_task) + ".sam")
				bamfile = os.path.join(self.bamdir, str(current_task) + ".bam")

				###process logging
				output.logging('# START TASK ' + str(current_task) + ' #', processlogfilename)
				if dataset[1]:
					output.logging("paired read data (forward): " + dstfiles[0] + "(downsampled " + srcfiles[0] + ")", processlogfilename)	
					output.logging("paired read data (reverse): " + dstfiles[1] + "(downsampled " + srcfiles[1] + ")", processlogfilename)	
				else:
					output.logging("unpaired read data: " + dstfiles[0] + "(downsampled " + srcfiles[0] + ")", processlogfilename)	
				output.logging("reference: " + refheader[1:] + "(" + reffilename + ')', processlogfilename)			
				
				###mapping		
				print("  mapping...")		
				output.logging('## MAPPING (Task ' + str(current_task) + ') ##', processlogfilename)
		
				if readpairing:
					refrank.mapping(reffilename, dstfiles[0], dstfiles[1], samfile, mapper, processlogfilename, cpus)			
				else:
					refrank.mapping(reffilename, dstfiles[0], False, samfile, mapper, processlogfilename, cpus)					
					
				#analysis
				print("  analyzing...")					
				#sam to bam
				output.logging('## SAM2BAM CONVERSION AND SORTING (Task ' + str(current_task) + ') ##', processlogfilename)
				refrank.sam2bam(samfile, bamfile, processlogfilename, cpus)
				
				#store bam file index info
				if keepBAM:
					bamfileindex.append([current_task, os.path.basename(bamfile), os.path.basename(reffilename), refheader[1:], " & ".join([os.path.basename(x) for x in dstfiles])])					
					
				#get coverages
				output.logging('## COVERAGE CALCULATIONS (Task ' + str(current_task) + ') ##', processlogfilename)
				
				self.coverages[srcfiles[0]][reffilename] = refrank.calcCoverage(bamfile, q, Q, processlogfilename, reflen, readnumber)
								
				msg = "norm_avg_base_cov: " + refrank.formatCov(self.coverages[srcfiles[0]][reffilename])
				print("  " + msg) 
				output.logging(msg, processlogfilename)
				
				output.logging('## CLEANING (Task ' + str(current_task) + ') ##', processlogfilename)
				#remove bam
				if not keepBAM:
					os.remove(bamfile)
					
				output.logging('# END TASK ' + str(current_task) + ' #', processlogfilename)

			#cleaning subset_files
			if not keepSubset:
				os.remove(dstfiles[0])
				if readpairing:
					os.remove(dstfiles[1])				
		
		#writing file reports
		if keepBAM:
			with open(os.path.join(self.resultdir, "bam_files.log"), "w") as handle:
				handle.write(output.formatAsCols(bamfileindex))
		if keepSubset:
			with open(os.path.join(self.resultdir, "fastq_files.log"), "w") as handle:
				handle.write(output.formatAsCols(fastqfileindex))
			
		#cleaning tmp dir
		shutil.rmtree(self.tmpdir)	

		#process logging end
		output.logging('# END LOG #', processlogfilename)		

		return True
		
		
	def createResultReport(self, mapper, fraction, silent=False):
		'''
		creates report of reference ranking 
		
		Input:
		mapper		used mapper (bwamem | bwasw)
		fraction	used percentage (float) or absolute read number (integer) for downsampling
		
		Return:
		True		
		'''
		
		if not silent:
			print('writing result report ...')
		
		reportfilename = os.path.join(self.resultdir, "refRanking.txt")

		
		#general info
		output.logging("REFRANK", reportfilename)
		output.logging("==============================", reportfilename)
		
		out = [["refRank version:", refrank.getVers()]]
		out.append(["bwa version:", refrank.getBwaVers()])
		out.append(["samtools version:", refrank.getSamtoolsVers()])
		out.append(["creation time:", self.resultdir.split("_")[-2] + " " + self.resultdir.split("_")[-1]])
		out.append(["mapping algorithm:", mapper])
		out.append(["base quality threshold:", self.q])	
		out.append(["mapping quality threshold:", self.Q])	
		if readpairing:
			out.append(["read type:", "paired-end reads"])
		else:
			out.append(["read type:", "single reads"])	
		if readpairing:
			count = len(self.datasets)/2
		else:
			count = len(self.datasets)
		out.append(["number of datasets:", count])	
		out.append(["number of fastq files:", str(len(self.datasets))])	
		out.append(["number of references:", str(len(self.references))])
		if isinstance(fraction, int):
			out.append(["downsampling:", str(fraction) + " reads"])
		else:
			out.append(["downsampling:", str(fraction*100) + "%"])
		
		out.append(["score:", "normalized and averaged per-base coverage"])

		output.logging(output.formatAsCols(out), reportfilename)
		output.logging('', reportfilename)
		output.logging('', reportfilename)
			
		#dataset info
		output.logging("DATASETS", reportfilename)
		output.logging("==============================", reportfilename)
		if readpairing:
			out = [["#", "forward read file", "reverse read file", "#reads per file", "#reads per file after downsampling"]]
		else:
			out = [["#", "read file", "#reads", "#reads after downsampling"]]
			
		i = 0
		for filename1, filename2, readcount in self.datasets:
			i += 1
			if filename2:
				out.append([i, filename1, filename2, readcount, refrank.calcSubsetsize(readcount, fraction)])
			else:
				out.append([i, filename1, readcount, refrank.calcSubsetsize(readcount, fraction)])
				
		output.logging(output.formatAsCols(out), reportfilename)
		output.logging('', reportfilename)
		output.logging('', reportfilename)
		
		#reference info
		output.logging("REFERENCES", reportfilename)
		output.logging("==============================", reportfilename)
		out = [["#", "file", "header", "sequence length"]]
		i = 0
		for filename, header, reflen in sorted(self.references, key=lambda x: x[0]):
			i += 1
			out.append([i, os.path.basename(filename), header[1:], reflen])
				
		output.logging(output.formatAsCols(out), reportfilename)
		output.logging('', reportfilename)
		output.logging('', reportfilename)		
		
		#Global Ranking
		output.logging("GLOBAL RANKING", reportfilename)
		output.logging("==============================", reportfilename)
		out = [["rank", "reference file", "reference header", "#best fits", "grand mean of scores"]]
		
		bestfits = []
		for filename1, filename2, readcount in self.datasets:
			bestfits.append(self.getBestFit(filename1))
		
		i = 0
		c = False
		for ref, avgcov in self.getGlobalRanking():
			if avgcov < c or c == False:
				i += 1
				c = avgcov
			header = [item[1] for item in self.references if item[0] == ref][0]
			out.append([i, os.path.basename(ref), header[1:], bestfits.count(ref), refrank.formatCov(avgcov)])
			
		output.logging(output.formatAsCols(out), reportfilename)
		output.logging('', reportfilename)
		output.logging('', reportfilename)

		#Dataset specific ranking
		output.logging("DATASET-SPECIFIC RANKING", reportfilename)
		output.logging("==============================", reportfilename)
		if readpairing:
			out = [["forward read file", "reverse read file"]]
		else:
			out = [["read file"]]
		
		i = 0
		for ref in self.references:
			i += 1
			out[0].append("rank " + str(i))
			
		for filename1, filename2, readcount in self.datasets:
			out.append([os.path.basename(filename1)])
			if readpairing:
				out[-1].append(os.path.basename(filename2))
				
			for ref, cov in self.getRanking(filename1):
				out[-1].append(os.path.basename(ref) + '(' + refrank.formatCov(cov) + ')')
		
		output.logging(output.formatAsCols(out), reportfilename)
		output.logging('', reportfilename)
		output.logging('', reportfilename)		

		#Coverages
		output.logging("SCORES", reportfilename)
		output.logging("==============================", reportfilename)
		if readpairing:
			out = [["forward read file", "reverse read file"]]
		else:
			out = [["read file"]]
		
			
		refs = sorted(self.references, key=lambda x: x[0])
		
		for filename, header, seqglen in refs:
			out[-1].append(os.path.basename(filename))		
		
		for filename1, filename2, readcount in sorted(self.datasets, key=lambda x: x[0]):
			out.append([os.path.basename(filename1)])
			if readpairing:
				out[-1].append(os.path.basename(filename2))
			for filename, header, seqlen in  refs:				
				out[-1].append(refrank.formatCov(self.getCov(ref=filename, dataset=filename1)))
				
		output.logging(output.formatAsCols(out), reportfilename)
		output.logging('', reportfilename)
		output.logging('', reportfilename)		

				
		#execution time		
		output.logging('--', reportfilename)	
		s = time.time() - self.inittime
		output.logging('execution time (hh:mm:ss): {:02.0f}:{:02.0f}:{:02.0f}'.format(s//3600, s%3600//60, s%60), reportfilename)			

	
	### format and round coverages ###
	@staticmethod	
	def formatCov(c):
		'''
		returns a formatted and rounded string of given coverage
		
		Input:
		c		coverage
		
		Output:
		formatted and rounded coverage (string)
		'''
	
		return '{:0.3e}'.format(c)
	
		
		
			
	## attributes ##
	def addRef(self, filename, silent = False):
		''' 
		adds FASTA file to reference collection by adding
		a tuple (FASTA filename, Header, Sequence length) to self.reference attribute
		and copying reference file to tmp dir
		
		Error message and exit is provoked when 
			- FASTA file contains more than one sequence entry
			- FASTA file contains no sequence entry	
			- FASTS files with same basenames

		Input:
		filename	name of FASTA file containing exactly one reference sequence
		silent		show no messages except exit errors (True | False)		
		
		Return:
		True
		'''
		
		i = 0
		for entry in refrank.readFasta(filename, silent):
			i += 1
					
		if i > 1:
			sys.exit("ERROR: " + filename + " contains multiple FASTA entries.")
					
		if i == 0:
			sys.exit("ERROR: " + filename + " contains no FASTA entry.")
			
		dstfilename = self.nameTmpFASTA(filename)
		
		if os.path.isfile(dstfilename):
			sys.exit("ERROR: multiple files named " + os.path.basename(filename))
		
		shutil.copyfile(filename, dstfilename)
		l = len(entry[1])
		if not silent:
			print("  " + str(l) + "nt")
		self.references.append((dstfilename, entry[0], l))
					
		return True
		
	def nameTmpFASTA(self, filename):
		'''
		returns the temporary filename + path for a given reference file
		'''
		
		return os.path.join(self.tmpdir, os.path.basename(filename))	
		
		
	def addFastq(self, filename, filename2 = False, silent = False):
		''' 
		adds FASTQ file to dataset collection by adding
		a tuple (FASTA forward filename, FASTA reverse filename, Read number) to self.datasets attribute
		
		In case of unpaired read data FASTA reverse filename is False
		
		Error message and exit is provoked when 
			- Basename of file(s) already in dataset collection
			- Forward and Reverse files contain different number of reads (in paired mode only)
			- readcount of at least one file is 0
		
		Input:
		filename	name of FASTQ file containing unpaired read information (unpaired mode)
					or name of FASTQ file containing forward read information (paired mode)
		filename2	False (unpaired mode)
					or name of FASTQ file containing reverse read information (paired mode)
		silent		show no messages except exit errors (True | False)	
		
		Return:
		True (adding was successfull)
		False (no adding since files were already in dataset collection)
		'''
		
		#add unpaired read data
		if not filename2:
			
			if filename in [x[0] for x in self.datasets]:
				return False
									
			if os.path.basename(filename) in [os.path.basename(x[0]) for x in self.datasets]:
				sys.exit("ERROR: multiple files are named " + os.path.basename(filename)) 
			
			readcount = refrank.countReads(filename, silent)
			
			if readcount == 0:
				sys.exit("ERROR: " + filename + " contains no read data")
				
			if not silent:
				print("  " + str(readcount) + " unpaired reads")
				
			self.datasets.append((filename, False, readcount))
		
		#add paired read data	
		else:
			
			if (filename, filename2) in [(x[0], x[1]) for x in self.datasets]:
				return False
				
			if os.path.basename(filename) in [os.path.basename(x[0]) for x in self.datasets]:
				sys.exit("ERROR: multiple files are named " + os.path.basename(filename)) 
				
			if os.path.basename(filename2) in [os.path.basename(x[1]) for x in self.datasets]:
				sys.exit("ERROR: multiple files are named " + os.path.basename(filename2)) 				
			
			readcount = refrank.countReads(filename, silent)			
			readcount2 = refrank.countReads(filename2, silent)
			
			if readcount == 0:
				sys.exit("ERROR: " + filename + " contains no read data")
				
			if readcount2 == 0:
				sys.exit("ERROR: " + filename2 + " contains no read data")				
			
			if readcount != readcount2:
				sys.exit("ERROR:" + filename + " and " + filename2 + " contain different numbers of reads") 
			
			if not silent:
				print("  " + str(readcount) + " paired reads")
				
			self.datasets.append((filename, filename2, readcount))
			
		return True
			
			
	def nameSubsetFASTQ(self, filename):
		'''
		returns the temporary filename + path for a given FASTQ file
		'''
		
		return os.path.join(self.fastqdir, re.sub(r'\.gzi?p?$', '', "subset_" + os.path.basename(filename)))			
			
	
	# reference index generation #
	@staticmethod
	def indexRef(filename, mapper, logfilename, silent = False):
		''' 
		generates a reference index using bwa and samtools 
		stdout and stderr are written to a given log file ('a' mode)
		
		Exit is provoked by bwa or samtools errors 
		
		Input:
		filename	name of FASTA file containing one reference sequence
		mapper		designated mapper (bwamem | bwasw)
		logfilename	name of log file stdout/stderr of bwa and samtools is appended to
		silent		show no messages except exit errors (True | False)
		
		Return:
		True
		'''
		if not silent:
			print("indexing ", os.path.basename(filename), "...")
		
		with open(logfilename, 'a') as lf:	
			if mapper in ['bwasw', 'bwamem']:
				cmd = ['bwa', 'index', filename]
			else:
				sys.exit('ERROR: unknown mapper')
			
			subprocess.check_call(cmd, shell=False, stdout=lf, stderr=lf)
			cmd = ['samtools', 'faidx', filename]
			subprocess.check_call(cmd, shell=False, stdout=lf, stderr=lf)	
		
		return True
	
	# read mapping #
	@staticmethod
	def mapping(ref, fastq, revfastq, samfilename, mapper, logfilename, cpus):
		'''
		read mapping using bwasw or bwamem
		stdout is written to a given file (samfilename)
		stderr is written to a given log file ('a' mode)
		
		If revfastq is False read data is considered to be unpaired/single
		
		Exit is provoked by bwa errors
		
		Input:
		ref			name of indexed FASTA file containing reference sequence
		fastq		name of FASTQ file containing unpaired read information (unpaired mode)
					or name of FASTQ file containing forward read information (paired mode)
		revfastq	False (unpaired mode)
					or name of FASTQ file containing reverse read information (paired mode)		
		samfilename	name of designated SAM file
		mapper		designated mapper (bwamem | bwasw)
		logfilename	name of log file stdout/stderr of bwa is appended to
		cpus		number of cpus to use
		
		Return:
		True
		
		'''
		
		with open(logfilename, 'a') as lf:	
			
			if mapper == 'bwasw':
				cmd = ['bwa', 'bwasw', '-t', str(cpus), ref,  fastq, revfastq, '-f', samfilename]
				if revfastq == False:
					del(cmd[6])
				subprocess.check_call(cmd, stderr=lf)	
				
			elif mapper == 'bwamem':
				with open(samfilename,"wb") as outfile:
					cmd = ['bwa', 'mem', '-t', str(cpus), ref,  fastq, revfastq]
					if revfastq == False:
						del(cmd[6])
					subprocess.check_call(cmd, stdout=outfile, stderr=lf)	
					
				
	# sam2bam format conversion #
	@staticmethod
	def sam2bam(samfilename, bamfilename, logfilename, cpus = 1, delSam = True):
		'''
		converts a SAM file to a sorted BAM file using samtools
		samfile is deleted by default after conversion (delSam = True)

		stdout is written to a given file (samfilename)
		stderr is written to a given log file ('a' mode)
		
		Exit is provoked by samtools errors
		
		Input:
		samfilename	name of the SAM file
		bamfilename	name of the designated BAM file
		logfilename	name of log file stderr of samtools is appended to
		cpus		number of cpus to use
		delSam		delete SAM file after conversion (True | False)
		
		Return:
		True
		'''
		
		#convert
		with open(bamfilename + ".tmp", 'w') as bf, open(logfilename, 'a') as lf:
			cmd = ['samtools', 'view', '-@', str(cpus), '-F', '4', '-Sb', samfilename]
			subprocess.check_call(cmd, stdout=bf, stderr=lf)
			
		#sort
		with open(bamfilename, 'w') as bf, open(logfilename, 'a') as lf:		
			cmd = ['samtools', 'sort', '-@', str(cpus), '-o', bamfilename, bamfilename + ".tmp"]
			subprocess.check_call(cmd, stdout=bf, stderr=lf)
			
		os.remove(bamfilename + ".tmp")
		if delSam:
			os.remove(samfilename)	
			
		return True
			
			
	# mapped read counter #		
	@staticmethod		
	def countMappedReads(bamfilename):	
		'''
		counts number of mapped reads using samtools flagstat command
		
		Exit is provoked by samtools errors
		
		Input:
		bamfilename	name of the BAM file
		
		Return:
		number (int) of mapped reads		
		'''
		
		cmd = ['samtools', 'flagstat', bamfilename]
		out = subprocess.check_output(cmd).decode("utf-8").split('\n')
		return int(out[2].split('+')[0])
	
	
	# calculate reference coverage #
	@staticmethod
	def calcCoverage(bamfilename, q, Q, logfilename, reflen = False, readnumber = False):		
		'''
		returns coverage (float) by assessing reference sequencing depth using samtools depth command and
		normalization to reference length (if reflen is not False) and read number (if readnumber is not False)

		Exit is provoked by samtools errors
		
		Input:
		bamfilename	name of the BAM file
		logfilename	name of log file stderr of samtools is appended to
		q			base quality threshold
		Q			mapping quality threshold
		reflen		length of used reference sequence (int)
					or False to not perform normalization based on reference length
		readnumber	number of mappend and unmapped reads in used dataset
					or False to not perform normalization based on read number
		
		Return:
		(normalized) coverage (float)	
		'''
		
		with open(logfilename, 'a') as lf:	
			cmd = ['samtools', 'depth', '-q', str(q), '-Q', str(Q), bamfilename]
			out = subprocess.check_output(cmd, stderr=lf).decode("utf-8").split('\n')
		cov = 0
		for line in out:
			line = line.strip()
			if line != "":
				cov += int(line.split("\t")[-1])
		
		if readnumber:
			cov = cov/readnumber
			
		if reflen:
			cov = cov/reflen			
			
		return cov
		

	# get version of bwa #
	@staticmethod
	def getBwaVers():	
		'''
		returns version of installed bwa (string)
		'''	
		
		cmd = ['bwa']
		process = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		lines = process.communicate()[1].decode("utf-8").split('\n')
		return lines[2].split(': ')[1]

		
	# get version of sammtools #
	@staticmethod	
	def getSamtoolsVers():	
		'''
		returns version of installed samtools (string)
		'''		
		
		cmd = ['samtools']
		process = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		lines = process.communicate()[1].decode("utf-8").split('\n')
		return lines[2].split(': ')[1]
		
	# FASTA reader #
	@staticmethod	
	def readFasta(filename, silent = False):
		'''
		creates a generator returning tuples of (header, sequence) obtained from a FASTA file. All lines are stripped.

		Input:
		filename	name of the FASTA file with one or more entries
		silent		show no messages (True | False)
		
		Return:
		generator of tuples (stripped header, stripped sequence) for each entry in the FASTA file
		'''
		
		if not silent:
			print("processing", filename, "...")
		
		with open(filename, 'r') as handle:
			
			#find first header:
			for line in handle:
				line = line.strip()
				
				if line[0] == ">":
					header = line
					break
		
			#process file after first header
			seq = []
			for line in handle:
				line = line.strip()

				if line == "" or line[0] == ";":
					continue
					
				elif line[0] == ">":
					entry = (header, "".join(seq))
					header = line
					seq = []
					yield entry
				
				else:
					seq.append(line)
					
		entry = (header, "".join(seq))
		yield entry
	
			
	# FastQ Reader #
	@staticmethod
	def readFastq(fastqfilename, linestripping = False, silent = False):
		'''
		creates a generator returning tuples of (identifier, description, read sequence, base quality scores) obtained from a FASTQ file.
		
		Exit is provoked by FASTQ format errors
		
		Input:
		filename		name of the FASTQ file
		linestripping	strip chars from both ends of lines (str)
						or False to not perform line stripping
		silent			show no messages (True | False)
		
		Return:
		generator of tuples (identifier, description, read sequence, base quality scores) for each read in the FASTQ file		
		'''
		
		#opening
		if not silent:
			print("processing", fastqfilename, "...")	
			
		mime = mimetypes.guess_type(fastqfilename)[1]
		if mime == 'gzip':
			handle = gzip.GzipFile(fastqfilename, 'rb')
		else:
			handle = open(fastqfilename, 'r')				
			
		#reading
		e = 0
		l = 0
		for line in handle: #don't use readline() since in case of gzip it is very slow
			l += 1 #counts line number
			e += 1 #counts line until 4 then starts at 1 to check entry lines
			
			if mime == 'gzip':
				line = line.decode('utf-8')		

			#identifier
			if e == 1:
				if line.strip() == "":
					e -= 1
					continue
				elif line[0] != "@":
					sys.exit("ERROR: " + fastqfilename + "is not a valid FASTQ file (identifier expected at line " + str(l) + ")")
			
				if linestripping:
					id = line.strip(linestripping)
					
				else:
					id = line
			
			#sequence
			elif e == 2:
				if line.strip() == "":	
					sys.exit("ERROR: " + fastqfilename + "is not a valid FASTQ file (sequence expected at line " + str(l) + ")")			

				if linestripping:
					seq = line.strip(linestripping)
				else:
					seq = line
					
			#description
			elif e == 3:
				if line[0] != "+":
					sys.exit("ERROR: " + fastqfilename + "is not a valid FASTQ file (description expected at line " + str(l) + ")")
				
				if linestripping:
					descr = line.strip(linestripping)
				else:
					descr = line					

			#base qualities
			else:
				if line.strip() == "":	
					sys.exit("ERROR: " + fastqfilename + "is not a valid FASTQ file (quality scores expected at line " + str(l) + ")")
			
				else:		
					e = 0
				
					if linestripping:
						qual = line.strip(linestripping)
					else:
						qual = line					
				
					yield (id, seq, descr, qual)
					
		if l < 4 and l > 0:
			sys.exit("ERROR: " + fastqfilename + "is not a valid FASTQ file (truncated entry at line " + str(l) + ")")
			
		handle.close()			
			
	# read counter in fastq file #
	@staticmethod
	def countReads(fastqfilename, silent = False):
		'''
		returns number of reads (integer) stored in a FASTQ file
		
		FASTQ format errors lead to error message and exit
		
		Input:
		filename		name of the FASTQ file
		silent			show no messages (True | False)
		
		Return:
		number of reads (int)
		'''

		if not silent:
			print("processing", fastqfilename, "...")
			
		readcount = 0
		for readdata in refrank.readFastq(fastqfilename, silent = True):
			readcount += 1
		
		if readcount == 0:
			sys.exit("ERROR: " + fastqfilename + "contains no data")

		return readcount
	
	# calculates the size after downsampling #
	@staticmethod					 
	def calcSubsetsize(readnumber, fraction):
		'''
		calculates given proportion of readnumber.
		
		if fraction is a float greater than 0 and less or equal to 1 it is handled as percentage
		if fraction is a integer it is handled as absolute read number (subset size)
		
		if fraction is integer and greater than readnumber, read number is returned

		Error message and exit are provoked by
			- fraction is float but less or equal to 0 or greater than 1
			- fraction is not float or integer

		Input:
		readnumber		number of total reads
		fraction		percentage (float) or absolute read number (integer)

		Return:
		number of subset reads (int)
		'''					 
	
		#percentage
		if isinstance(fraction, float):
			if fraction <= 0 or fraction > 1:
				sys.exit("fraction has to be a greather than 0 and lower or equal to 1")
			return int(readnumber * fraction)
		
		#absolute number
		elif isinstance(fraction, int):
			if fraction > readnumber:
				return readnumber
			else:
				return fraction
		
		#unknown type (error)				 
		else:
			sys.exit("ERROR: fraction has to be a percentage (float) or absolute read number (integer)")
					 
					 
	# random subset creation from fastq file #
	@staticmethod
	def createFastqSubset(srcfiles, dstfiles, fraction, totalreadnumber = False, silent = False):
		'''
		downsamples FASTQ file by extracting a subset of randomly picked reads. In case of paired read data, read pairing is conserved.
		
		Error message and exit are provoked by
			- FASTQ format errors 
			- unmatching number of srcfiles and dstfiles
		
		Input:
		srcfiles		list of names of FASTQ file (paired mode: 2 names, single mode: 1 name)
		dstfiles		list of names of designated subset FASTQ files in order of srcfiles (paired mode: 2 names, single mode: 1 name)
		fraction		percentage (float) or absolute read number (integer)
						integeres will be handled as absolute read numbers
		totalreadnumber	number of reads per srcfile										
		silent			show no messages execpt error messages (True | False)
		
		Return:
		number of subset reads (int)
		'''
		
		#preparing
		if len(srcfiles) > 2 or len(srcfiles) < 1:
			sys.exit("ERROR: illegal number of FASTQ files (only 1 [single] or 2 [paired] allowed)")
			
		if len(srcfiles) != len(dstfiles):
			sys.exit("ERROR: Number of source and subset files vary")
			
		if len(srcfiles) == 1 and silent == True:
			print("  file:", srcfiles[0])
			print("  read count:", totalreadnumber)
		
		elif len(srcfiles) == 2 and silent == True:
			print("  forward file:", srcfiles[0])
			print("  reverse file:", srcfiles[1])
			print("  read count per file:", totalreadnumber)		
			
		
		print("  downsampling ...")
				
		if not totalreadnumber:
			totalreadnumber = refrank.countReads(fastqfilename)

		newreadnumber = refrank.calcSubsetsize(totalreadnumber, fraction)
		
		selected = set(random.sample(range(1, totalreadnumber+1), newreadnumber))
		maximal = max(selected)
			
		#downsampling
		f = -1 #file counter
		for srcfile in srcfiles:
			if not silent:
				print("    " + os.path.basename(srcfile))
			f += 1
			stored = 0			
			p = 0
			dstfile = dstfiles[f]
			with open(dstfile, 'w') as dsthandle:
				for readdata in refrank.readFastq(srcfile, silent = True):
					p += 1
					if p in selected:
						dsthandle.write("".join(readdata))
						if p == maximal:
							break

		return newreadnumber
	
	def getCov(self, ref = False, dataset = False):
		'''
		get all coverages of a reference and/or dataset

		Input:
		ref		name of reference FASTA file 
				or False
		dataset	name of dataset FASTQ file
				or False
		
		Return:
		A) if ref not False and dataset not False
		(normalized) coverage of after mapping dataset to reference (float)
		
		B) if ref not False and dataset False
		dictionary of {dataset: (normalized) coverage} after mapping each existing dataset to reference (dict)
		
		C) if ref False and dataset not False
		dictionary of {ref: (normalized) coverage} after mapping dataset to each existing reference (dict)
		
		D) if ref False and dataset False:
		nested dictionary of {dataset: {ref: (normalized) coverage}} after mapping each dataset to each existing reference (dict)		
		'''	
		
		if ref and dataset:
			return self.coverages[dataset][ref]
		elif ref:
			covs = {}
			for dataset in self.coverages:
				covs[dataset] = self.coverages[dataset][ref]
			return covs
		elif dataset:
			covs = {}
			for ref in self.coverages[dataset]:
				covs[ref] = self.coverages[dataset][ref]	
			return covs			
		else:
			return self.coverages

	
	def getRanking(self, dataset):
		'''
		get tuples (file names of references,  coverage) sorted from best (highest sum of mapped reads) to lowest rank based on a specific datasets
		
		Input:
		dataset	file name of datatset
		
		Return:
		tuples (file names of references, coverage) of of best to lowest-ranked reference (list)
		'''					 

		covs = {}			 
		
		for ref in self.coverages[dataset]:
			covs[ref] = self.getCov(ref, dataset)
					 
		return sorted(covs.items(), key=lambda x:x[1], reverse=True)
					 
					 
	def getGlobalRanking(self):
		'''
		get tuples (file names of references,  mean %coverage) sorted from best (highest sum of mapped reads) to lowest rank based on all datasets
		
		Return:
		tuples (file names of references,  mean %coverage) of of best to lowest-ranked reference (list)
		'''	
		
		covs = {} 
		
		reffilenames = [x[0] for x in self.references]
		datasetfilenames = [x[0] for x in self.datasets]			 
		
		for ref in reffilenames:
			for dataset in datasetfilenames:
				if not ref in covs:
					covs[ref] = self.getCov(ref, dataset)
				else:
					covs[ref] += self.getCov(ref, dataset)
			covs[ref] = covs[ref] / len(datasetfilenames)
					
		return sorted(covs.items(), key=lambda x:x[1], reverse=True)
					 
			
	def getBestFit(self, dataset):
		'''
		get file name of the best-fitting reference (1st rank) to a given dataset
		
		Input:
		dataset	file name of dataset

		Return:
		file name of reference best-fitting to the given dataset		
		'''	
		
		covs = self.getCov(dataset = dataset)
		#return sorted(covs.items(), key=lambda x:x[1], reverse=True)[0]
		return sorted(covs, key=covs.get, reverse=True)[0]
		
	@staticmethod	
	def getVers():
		version = '1.0.0'
		return version
		
	def setTraceless(mode):
		if not type(mode) is bool:
			return False
		
		self.traceless = mode
		return True

	def delTraces(self):
		folders = []
		if hasattr(self, 'resultdir'):
			folders.append(self.resultdir)
		if hasattr(self, 'tmpdir'):
			folders.append(self.tmpdir)
		if hasattr(self, 'bamdir'):
			folders.append(self.bamdir)
		if hasattr(self, 'fastqdir'):
			folders.append(self.fastqdir)
			
		for folder in folders:
			if os.path.isdir(folder):
				shutil.rmtree(folder)

	def __exit__(self):
		if self.traceless:
			self.delTraces()			
					
		
		
# MAIN #	
if __name__ == "__main__":
	import argparse
	
	#cli argument
	parser = argparse.ArgumentParser(prog="REFRANK", description='ranking references based on normalized per-base coverage')
	parser.add_argument('-f', '--fastq', metavar="FILE", help="FASTQ file(s) containing read data", type=argparse.FileType('r'), required=True, nargs='+')
	parser.add_argument('-r', '--ref', metavar="FILE", help="FASTA file(s) containing one reference sequence each", type=argparse.FileType('r'), required=True, nargs='+')
	selectionbase = parser.add_mutually_exclusive_group()
	selectionbase.add_argument('-p', metavar="DEC", help="proportion of reads used for mapping greater than 0 and less or equal than 1 (default: 0.1)",  type=float, default=0.1)
	selectionbase.add_argument('-n', metavar="INT", help="absolute number of reads used for mapping.",  type=int, default=False)
	parser.add_argument('-t', metavar='INT', help="number of threads/CPUs (default: 20)", type=int, action="store", default=20)
	parser.add_argument('-k', help="keep subset read files (FASTQ format). Warning: Large amount of data may be generated!",  action="store_true")
	parser.add_argument('-b', help="keep mapping alignment files files (BAM format). Warning: Large amount of data may be generated!",  action="store_true")
	parser.add_argument('-q', metavar='INT', help="base quality threshold (default: 20)",  type=int, default=20)
	parser.add_argument('-Q', metavar='INT', help="mapping quality threshold (default: 20)",  type=int, default=20)
	parser.add_argument('--mode', help="mode: use paired for paired-end or single for unpaired read data (default: paired)",  choices=['paired', 'single'], action="store", default='paired')
	parser.add_argument('--mapper', help="select mapping algorithm (default: bwamem).",  choices=['bwasw', 'bwamem'], action="store", default='bwamem')
	parser.add_argument('-d', '--dir', metavar='STR', help="name of the output directory (has to be empty or not existent). By default a directory named 'REFRANK_' followed by an timestamp is created to save result files.", type=str, default=False)
	parser.add_argument('--version', action='version', version='%(prog)s ' + refrank.getVers())
	args = parser.parse_args()
	
	#argument checking	
	if args.mode == "paired":
		readpairing = True
	else:
		readpairing = False
	
	if args.p and args.p > 1:
		sys.exit("ERROR: value of argument -p cannot be greater than 1")
	if args.p and args.p <= 0:
		sys.exit("ERROR: value of argument -p cannot be less or equal to 0")
		
	if args.n and args.n <= 0:
		sys.exit("ERROR: value of argument -n cannot be less or equal to 0")
		
	if args.q < 0:
		sys.exit("ERROR: value of argument -q cannot be less than 0")

	if args.Q < 0:
		sys.exit("ERROR: value of argument -Q cannot be less than 0")
	
	
	if args.n:
		fractionsize = args.n
	else:
		fractionsize = args.p
		
				
	#processing filenames
	reffilenames = [x.name for x in args.ref]
	fastqfilenames = [x.name for x in args.fastq]
	for handle in args.ref:
		handle.close()
	for handle in args.fastq:
		handle.close()
		
	rankobj = refrank(reffilenames, fastqfilenames, readpairing, fractionsize, args.mapper, args.q, args.Q, args.b, args.k, args.dir, cpus = args.t, silent = False)